//
//  NetworkServiceTests.swift
//  JustParkTest
//
//  Created by Carla on 2017/08/18.
//  Copyright © 2017 Carla. All rights reserved.
//

import XCTest
@testable import JustParkTest

class NetworkServiceTests: XCTestCase {
    var subject: NetworkService!
    
    override func setUp() {
        super.setUp()
        subject = NetworkService()
    }
    
    func test_GET_ReturnsData() {
        let url = URL(string: "http://masilotti.com")!
        let expection_1 = expectation(description: "Wait for \(url) to load.")
        var data: [ParkingSpace]?
        
        subject.retrieveSearchResults { (results, error) in
            data = results
            expection_1.fulfill()
        }
        
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertNotNil(data)
    }
    
}
