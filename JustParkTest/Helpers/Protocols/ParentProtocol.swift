//
//  ParentProtocol.swift
//  JustParkTest
//
//  Created by Carla on 2017/08/20.
//  Copyright © 2017 Carla. All rights reserved.
//

import Foundation
import UIKit

protocol ParentProtocol {
    var heightScalingFactor : CGFloat {get}
    func add(asChildViewController viewController: UIViewController?)
    func remove(asChildViewController viewController: UIViewController?, completion: (()->())?)
}

extension ParentProtocol where Self: UIViewController {
    
    var heightScalingFactor : CGFloat { get { return 3.0 }}
    
    func add(asChildViewController viewController: UIViewController?) {
        guard viewController != nil else {
            return
        }
        
        //Add child
        addChildViewController(viewController!)
        view.addSubview(viewController!.view)
        
        //Setup Start Frame for animation
        var startFrame = view.bounds
        startFrame.size = CGSize(width:view.bounds.size.width, height: view.bounds.size.height/heightScalingFactor)
        startFrame.origin = CGPoint(x: 0, y: view.bounds.size.height)
        
        //Setup End Frame for animation
        var endFrame = view.bounds
        endFrame.size = CGSize(width:view.bounds.size.width, height: view.bounds.size.height/heightScalingFactor)
        endFrame.origin = CGPoint(x: 0, y: (view.bounds.size.height - view.bounds.size.height/heightScalingFactor))
    
        viewController!.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        viewController!.view.frame = startFrame
    
        UIView.animate(withDuration: 0.3, delay: 0.0, options: .allowAnimatedContent, animations: {
            viewController!.view.frame = endFrame
        }) { (finished) in
            viewController!.didMove(toParentViewController: self)
        }
        
    }
    
    func remove(asChildViewController viewController: UIViewController?, completion: (()->())?) {
        guard viewController != nil else {
            completion?()
            return
        }
        
        //Setup End Frame for animation
        var endFrame = view.bounds
        endFrame.size = CGSize(width:view.bounds.size.width, height: view.bounds.size.height/heightScalingFactor)
        endFrame.origin = CGPoint(x: 0, y: view.bounds.size.height)
        
        UIView.animate(withDuration: 0.3, delay: 0.0, options: .allowAnimatedContent, animations: {
            viewController!.view.frame = endFrame
        }) { (finished) in
            viewController!.willMove(toParentViewController: nil)
            
            // Remove Child View From Superview
            viewController!.view.removeFromSuperview()
            viewController!.removeFromParentViewController()
            completion?()
        }
        
    }
}
