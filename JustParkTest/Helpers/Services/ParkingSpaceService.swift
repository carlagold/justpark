//
//  ParkingSpaceService.swift
//  JustParkTest
//
//  Created by Carla on 2017/08/18.
//  Copyright © 2017 Carla. All rights reserved.
//

import Foundation
import CoreLocation

class ParkingSpaceService {
    //MARK: - Properties
    var parkingSpaces: [ParkingSpace] = []
    var networkService: NetworkService?
    
    //MARK: - Initialize
    init() {
        self.networkService = NetworkService()
    }
    
    //MARK: - Public API
    func getParkingSpaces(parameters: JSONDictionary, completion: @escaping ([ParkingSpace]) -> Void) {

        self.networkService?.retrieveSearchResults(parameters:parameters) { responseJSON, error in
            self.parkingSpaces.removeAll()
            
            guard let parkingSpacesDictionaries = responseJSON!["data"] as? [Any] else {
                return
            }
            
            
            for dictionary in parkingSpacesDictionaries {
                self.parseDictionary(dictionary: dictionary)
            }
            
            DispatchQueue.main.async {
                completion(self.parkingSpaces)
            }
        }
    }
    
    //MARK: - Private
    fileprivate func parseDictionary(dictionary: Any)  {
        if let dictionary = dictionary as? JSONDictionary,
            let sourceId = dictionary["id"] as? Int,
            let url = dictionary["url"] as? String,
            let distance = dictionary["distance"] as? Double,
            let title = dictionary["title"] as? String,
            let description = dictionary["description"] as? String,
            let currencyDict = dictionary["currency"] as? JSONDictionary,
            let currencyCode = currencyDict["code"] as? String,
            let currencySymbol = currencyDict["symbol"] as? String,
            let currency = (symbol: currencySymbol, code: currencyCode) as? Currency,
            let price = dictionary["price"] as? Double,
            let locationDict = dictionary["location"] as? JSONDictionary,
            let longitude = locationDict["longitude"] as? Double,
            let latitude = locationDict["latitude"] as? Double,
            let review = dictionary["review_average"] as? Double,
            let reviewCount = dictionary["review_count"] as? Int {
            parkingSpaces.append(ParkingSpace(sourceId: sourceId,
                                              url: url,
                                              distance: distance,
                                              title: title,
                                              description: description,
                                              currency: currency,
                                              price: price,
                                              location: CLLocationCoordinate2DMake(latitude, longitude),
                                              review: review,
                                              reviewCount: reviewCount))
        } else {
           print("Problem parsing trackDictionary\n")
        }
        
    }
    
    
}
