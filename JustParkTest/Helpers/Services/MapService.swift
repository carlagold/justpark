//
//  MapService.swift
//  JustParkTest
//
//  Created by Carla on 2017/08/18.
//  Copyright © 2017 Carla. All rights reserved.
//

import Foundation
import GoogleMaps
import CoreLocation


typealias MapConfig = (latitude: Double, longitude: Double, distance: Double)

struct MapService {
    //MARK: - Properties
    static let key = "AIzaSyBir1YIZ_8dAR4dEE5YGAZvXQ7DayLAFlU"
    var networkService: NetworkService?
    var config : MapConfig  { get { return (latitude: 51.5560241, longitude: -0.2817075, distance: 100)}}

    //MARK: - Initialize
    init() {
        self.networkService = NetworkService()
    }
    
    //MARK: - Public API
    mutating func drawPath(origin: CLLocationCoordinate2D, destination: CLLocationCoordinate2D, completion: @escaping (GMSPolyline) -> Void)
    {
        let location1 = "\(origin.latitude),\(origin.longitude)"
        let location2 = "\(destination.latitude),\(destination.longitude)"
        
        if let url = URL(string:"https://maps.googleapis.com/maps/api/directions/json?origin=\(location1)&destination=\(location2)&mode=walking&key=\(MapService.key)") {
            self.networkService?.requestData(url:url) { responseJSON, error  in
                
                let routes = responseJSON?["routes"] as? [JSONDictionary]
                
                for route in routes!
                {
                    let routeOverviewPolyline = route["overview_polyline"] as? [String: Any]
                    let points = routeOverviewPolyline?["points"] as? String
                    let path = GMSPath.init(fromEncodedPath: points!)
                    let polyline = GMSPolyline.init(path: path)
                    
                    DispatchQueue.main.async {
                        completion(polyline)
                    }
                }
                
            }
        }
    }
    
}
