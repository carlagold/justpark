//
//  NetworkService.swift
//  JustParkTest
//
//  Created by Carla on 2017/08/18.
//  Copyright © 2017 Carla. All rights reserved.
//

import Foundation

typealias JSONDictionary = [String: Any]
typealias QueryResult = (JSONDictionary?, String) -> ()


class NetworkService {
    //MARK: - Properties
    let defaultSession = URLSession(configuration: .default)
    var dataTask: URLSessionDataTask?
    var errorMessage = ""
    
    //MARK: - Public API
    func requestData(url: URL, completion: @escaping QueryResult) {
        dataTask?.cancel()
        
        dataTask = defaultSession.dataTask(with: url) { data, response, error in
            defer { self.dataTask = nil }
            if let error = error {
                self.errorMessage += "DataTask error: " + error.localizedDescription + "\n"
            } else if let data = data,
                let response = response as? HTTPURLResponse,
                response.statusCode == 200 {
                let responseJSON = self.parseDataToJSON(data)
                DispatchQueue.main.async {
                    completion(responseJSON, self.errorMessage)
                }
            }
 
        }
        dataTask?.resume()

    }
    
    func retrieveSearchResults(parameters: JSONDictionary, completion: @escaping QueryResult) {
        dataTask?.cancel()
        
        guard let url = URL(string:"https://api.justpark.com/apiv3/search/region") else {return}
        
        let request = createPostRequest(url: url, parameters: parameters)
        
        dataTask = defaultSession.dataTask(with: request) { data, response, error in
            defer { self.dataTask = nil }
            if let error = error {
                self.errorMessage += "DataTask error: " + error.localizedDescription + "\n"
            } else if let data = data,
                let response = response as? HTTPURLResponse,
                response.statusCode == 200 {
                let responseJSON = self.parseDataToJSON(data)
                DispatchQueue.main.async {
                    completion(responseJSON, self.errorMessage)
                }
            }
        }
        dataTask?.resume()
    }
    

    //MARK: - Private
    fileprivate func parseDataToJSON(_ data: Data) -> JSONDictionary? {
        var response: JSONDictionary?
        
        do {
            response = try JSONSerialization.jsonObject(with: data, options: []) as? JSONDictionary
        } catch let parseError as NSError {
            errorMessage += "JSONSerialization error: \(parseError.localizedDescription)\n"
            return nil
        }
        
        return response

    }
    
    fileprivate func createPostRequest(url: URL, parameters: JSONDictionary) -> URLRequest {
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
        } catch let error {
            print(error.localizedDescription)
        }
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        return request

    }
}


