//
//  MarkerView.swift
//  JustParkTest
//
//  Created by Carla on 2017/08/20.
//  Copyright © 2017 Carla. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class MarkerView : UIView {

    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var markerBackground: UIView!
    @IBOutlet weak var markerPrice: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func config(price: String) {
        self.markerPrice.text = "£"  + price
    }
    
    func selected() {
        self.backgroundImage.image = UIImage(named: "speechbubble_selected")
        self.markerPrice.textColor = .white
    }
    
    func deselected() {
        self.backgroundImage.image = UIImage(named: "speechbubble")
        self.markerPrice.textColor = UIColor(colorLiteralRed: 38/255, green: 177/255, blue: 62/255, alpha: 1.0)
    }
    
    
}

