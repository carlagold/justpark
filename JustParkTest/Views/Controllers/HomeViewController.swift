 //
 //  FirstViewController.swift
 //  JustParkTest
 //
 //  Created by Carla on 2017/08/17.
 //  Copyright © 2017 Carla. All rights reserved.
 //
 
 import UIKit
 import GoogleMaps
 
 final class HomeViewController: UIViewController, ParentProtocol {
    //MARK: - Properties
    var viewModel : HomeViewModel?
    var polyline : GMSPolyline?
    var selectedMarker : GMSMarker?
    fileprivate var moreInfoViewController: MoreInfoViewController?
    
    //MARK: - Setup
    override func loadView() {
        super.loadView()
        let reloadCallback : (()->()) = { [weak self] in
            self?.reload()
        }
        self.viewModel = HomeViewModel(reloadCallback: reloadCallback)
        self.setup()
    }
    
    func setup() {
        let camera = GMSCameraPosition.camera(withLatitude:self.viewModel!.mapConfigurationLatitude, longitude: self.viewModel!.mapConfigurationLongitude, zoom: 14.0)
        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        mapView.delegate = self
        mapView.settings.compassButton = true
        mapView.settings.myLocationButton = true
        
        view = mapView
        self.addStartLocation()
    }
    
    func addStartLocation() {
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2DMake(self.viewModel!.mapConfigurationLatitude, self.viewModel!.mapConfigurationLongitude)
        marker.appearAnimation = GMSMarkerAnimation.pop
        marker.isTappable = false
        marker.map = view as? GMSMapView
    }
    
    //MARK: - Load Parkings
    func reload() {
        self.loadParkingSpots()
    }

    func loadParkingSpots() {
        for index in 0..<(self.viewModel?.totalNumberOfParkingSpaces)! {
            self.addPinForParkingSpace(atIndex: index)
        }
    }
    
    func addPinForParkingSpace(atIndex index: Int) {
        let marker = GMSMarker()
        
        let markerView = Bundle.main.loadNibNamed("MarkerView", owner: nil, options: nil)?[0] as! MarkerView
        markerView.config(price: self.viewModel!.priceForParkingSpace(atIndex: index)!)
        marker.iconView = markerView
        marker.position = (self.viewModel?.positionForParkingSpace(atIndex: index))!
        marker.appearAnimation = GMSMarkerAnimation.pop
        marker.tracksViewChanges = false
        
        marker.map = view as? GMSMapView
    }
 }
 
 //MARK: - GMSMapViewDelegate

 extension HomeViewController : GMSMapViewDelegate {
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        let camera = GMSCameraPosition.camera(withLatitude:self.viewModel!.mapConfigurationLatitude, longitude: self.viewModel!.mapConfigurationLongitude, zoom: 14.0)
        let update = GMSCameraUpdate.setCamera(camera)
        mapView.animate(with: update)
        return false
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
      self.hideParkingSpaceDetailsIfNeeded()
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        self.drawPath(from: marker) {
            self.showParkingSpaceDetails(for: marker)
            self.selectMarker(mapView: mapView, marker: marker)
        }
        
        return true
    }
    
    //MARK: GMSMapViewDelegate Helpers
    func showParkingSpaceDetails(for marker: GMSMarker) {
        //TODO: find a better way to identify a marker. There might be 2 markers at one position
        self.remove(asChildViewController: moreInfoViewController) {
            self.moreInfoViewController = MoreInfoViewController(nibName: "MoreInfoViewController",
                                                                 bundle:Bundle.main,
                                                                 viewModel: (self.viewModel?.nextViewModel(forMarkerAtPosition: marker.position))!)
            self.add(asChildViewController: self.moreInfoViewController!)
        }
    }
    
    func hideParkingSpaceDetailsIfNeeded() {
        guard moreInfoViewController != nil else { return}
        
        self.remove(asChildViewController: moreInfoViewController!, completion: nil)
    }
    
    func drawPath(from marker:GMSMarker, completion: @escaping (()->())) {
        let origin = CLLocationCoordinate2DMake(self.viewModel!.mapConfigurationLatitude, self.viewModel!.mapConfigurationLongitude)
        self.viewModel?.getPathBetween(origin:marker.position , destination: origin) { polyline in
            self.polyline?.map = nil
            self.polyline = polyline
            self.polyline?.map = self.view as? GMSMapView
            completion()
        }
    }
    
    func selectMarker(mapView: GMSMapView, marker: GMSMarker) {
        self.deselectMarker()
        
        guard let markerView = marker.iconView as? MarkerView else {return}
        
        marker.tracksViewChanges = true
        markerView.selected()
        marker.iconView = markerView
        marker.zIndex = 1
        self.selectedMarker = marker
        
        marker.tracksViewChanges = false

    }
    
    func deselectMarker() {
        
        guard let marker = self.selectedMarker else {return}
        
        marker.tracksViewChanges = true
        let markerView = marker.iconView as! MarkerView
        markerView.deselected()
        marker.iconView = markerView
        marker.zIndex = 0

        marker.tracksViewChanges = false
    }
    
 }
 
 
 
 
 
