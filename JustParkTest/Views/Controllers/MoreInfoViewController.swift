//
//  MoreInfoViewController.swift
//  JustParkTest
//
//  Created by Carla on 2017/08/20.
//  Copyright © 2017 Carla. All rights reserved.
//

import Foundation
import UIKit

class MoreInfoViewController : UIViewController {
    
    //MARK: - Properties
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var ratingCount: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var star5: UIImageView!
    @IBOutlet weak var star4: UIImageView!
    @IBOutlet weak var star3: UIImageView!
    @IBOutlet weak var star1: UIImageView!
    @IBOutlet weak var star2: UIImageView!
    @IBOutlet weak var parkingSpaceTitle: UILabel!
    var viewModel : MoreInfoViewModel?
    
    //MARK: - Initialize
    init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?,  viewModel: MoreInfoViewModel) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.viewModel = viewModel
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    //MARK: - Setup View
    override func viewDidLoad() {
        super.viewDidLoad()
        self.parkingSpaceTitle.text = self.viewModel?.title
        self.configStarRating(rating: (self.viewModel?.review)!)
        self.ratingCount.text = "(\((self.viewModel?.reviewCount)!))"
        self.distanceLabel.text = String(format: "%.2f miles", (self.viewModel?.distance)!) // "3.14"  String(round((self.viewModel?.distance)!*100)) + " miles"
        self.descriptionTextView.text = self.viewModel?.description
    }
    
    
    //MARK: Helpers

    func configStarRating(rating: Double) {
        star1.image = getStarImage(1, forRating: rating)
        star2.image = getStarImage(2, forRating: rating)
        star3.image = getStarImage(3, forRating: rating)
        star4.image = getStarImage(4, forRating: rating)
        star5.image = getStarImage(5, forRating: rating)
    }
    
    
    func getStarImage(_ starNumber: Double, forRating rating: Double) -> UIImage {
        let fullStarImage:  UIImage = UIImage(named: "starfull")!
        let halfStarImage:  UIImage = UIImage(named: "starhalf")!
        let emptyStarImage: UIImage = UIImage(named: "star")!
        
        if rating >= starNumber {
            return fullStarImage
        } else if rating + 0.5 == starNumber {
            return halfStarImage
        } else {
            return emptyStarImage
        }
    }
    
}
