//
//  ParkingSpace.swift
//  JustParkTest
//
//  Created by Carla on 2017/08/18.
//  Copyright © 2017 Carla. All rights reserved.
//

import Foundation
import CoreLocation

typealias Currency = (symbol: String, code: String)

struct ParkingSpace {
    
    let sourceId : Int
    let url: String
    let distance: Double
    let title: String
    let description: String
    let currency : Currency
    let price : Double
    let location : CLLocationCoordinate2D
    let review: Double
    let reviewCount: Int
    
    init(sourceId: Int,
         url: String,
         distance: Double,
         title: String,
         description: String,
         currency: Currency,
         price: Double,
         location: CLLocationCoordinate2D,
         review: Double,
         reviewCount: Int) {
        self.sourceId = sourceId
        self.url = url
        self.distance = distance
        self.title = title
        self.description = description
        self.currency = currency
        self.price = price
        self.location = location
        self.review = review
        self.reviewCount = reviewCount
    }
}
