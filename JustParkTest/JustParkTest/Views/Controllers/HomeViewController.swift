 //
 //  FirstViewController.swift
 //  JustParkTest
 //
 //  Created by Carla on 2017/08/17.
 //  Copyright © 2017 Carla. All rights reserved.
 //
 
 import UIKit
 import GoogleMaps
 
 class HomeViewController: UIViewController {
    
    var viewModel : HomeViewModel?
    
    override func loadView() {
        let reloadCallback : (()->()) = { [weak self] in
            self?.reload()
        }
        self.viewModel = HomeViewModel(reloadCallback: reloadCallback)
        self.setup()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func reload() {
        self.loadParkingSpots()
    }
 }
 
 extension HomeViewController {
    
    func setup() {
        // Create a GMSCameraPosition that tells the map to display
        let camera = GMSCameraPosition.camera(withLatitude:self.viewModel!.mapConfigurationLatitude, longitude: self.viewModel!.mapConfigurationLongitude, zoom: 14.0)
        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        view = mapView
        self.addStartLocation()
    }
    
    
    func loadParkingSpots() {
        for index in 0..<(self.viewModel?.totalNumberOfParkingSpaces)! {
            self.addPinForParkingSpace(atIndex: index)
        }
        
        let origin = CLLocationCoordinate2DMake(self.viewModel!.mapConfigurationLatitude, self.viewModel!.mapConfigurationLongitude)
        self.viewModel?.getPathBetween(origin: origin, destination: (self.viewModel?.positionForParkingSpace(atIndex: 4))!) { polyline in
            polyline.map = self.view as? GMSMapView
   
        }
        
    }
    
    func addStartLocation() {
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2DMake(self.viewModel!.mapConfigurationLatitude, self.viewModel!.mapConfigurationLongitude)
        marker.title = "You are here"
        marker.snippet = ""
        marker.appearAnimation = GMSMarkerAnimation.pop
        marker.icon = GMSMarker.markerImage(with: .black)
        
        marker.map = view as? GMSMapView
    }
    
    func addPinForParkingSpace(atIndex index: Int) {
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = (self.viewModel?.positionForParkingSpace(atIndex: index))!
        marker.title = self.viewModel?.titleForParkingSpace(atIndex: index)
        marker.snippet = self.viewModel?.descriptionForParkingSpace(atIndex: index)
        marker.appearAnimation = GMSMarkerAnimation.pop
        marker.icon = GMSMarker.markerImage(with: .green)
        
        marker.map = view as? GMSMapView
    }
 }
 
 
