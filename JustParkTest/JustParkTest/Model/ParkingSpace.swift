//
//  ParkingSpace.swift
//  JustParkTest
//
//  Created by Carla on 2017/08/18.
//  Copyright © 2017 Carla. All rights reserved.
//

import Foundation
import CoreLocation

typealias Currency = (symbol: String, code: String)

struct ParkingSpace {
    
    
    let sourceId : Int
    let url: String
    let distance: Double
    let title: String
    let description: String
    let currency : Currency
    let location : CLLocationCoordinate2D
 
    init(sourceId: Int,
         url: String,
         distance: Double,
         title: String,
         description: String,
         currency: Currency,
         location: CLLocationCoordinate2D) {
        self.sourceId = sourceId
        self.url = url
        self.distance = distance
        self.title = title
        self.description = description
        self.currency = currency
        self.location = location
    }
    
}

/*
 "id": 162343,
 "url": "/uk/parking/wembley/s/empire-way-wembley-ha9-0rj-p579634d35cc1b/",
 "distance": 0.22369362912,
 "title": "Car Park on Empire Way, HA9",
 "custom_title": "Car Park on Empire Way, HA9",
 "currency": {
 "symbol": "£",
 "code": "GBP"
 },
 "price": 1,
 "price_hour": 1,
 "price_day": 35,
 "price_week": 110,
 "price_month": 415,
 "address_lat": 51.557341,
 "address_lng": -0.286479,
 "review_count": 542,
 "review_average": 4.56,
 "address_1": "Not required",
 "address_2": "Not required",
 "address_3": "Not required",
 "postal_code": "Not required",
 "cancellation_policy": "https://api.justpark.com/api/v4/content/cancellation-policy/",
 "cancellation_policy_type": "flexible",
 "facilities": [
 {
 "id": "has_security_guards",
 "title": "Guarded",
 "description": "Has security guards.",
 "is_restricted": false
 },
 {
 "id": "has_multiple_entry",
 "title": "Multiple entry and exit permitted",
 "description": "Multiple entry and exit permitted",
 "is_restricted": false
 }
 ],
 "integration_partner": {
 "is_partner": false,
 "partner": null,
 "company_name": null,
 "entry_method": "ANPR"
 },
 "location": {
 "latitude": 51.557341,
 "longitude": -0.286479
 },
 "category": "Private parking lot",
 "quantity": 75,
 "review": {
 "id": 512993,
 "user_id": 1390413,
 "name": "Cassandra M",
 "user_avatar_url": "https://graph.facebook.com/10155493914982278/picture?width=200",
 "created_at": "2017-08-18 07:50:14",
 "rating": 5,
 "comment": null
 },
 "gallery": [
 {
 "thumbnail": {
 "url": "https://uploads.justpark.com/cdn/media/uploaded/listing-photos/57b1d25f235e3-thumb.jpg",
 "width": 120,
 "height": 120
 },
 "normal": {
 "url": "https://uploads.justpark.com/cdn/media/uploaded/listing-photos/57b1d25f235e3-normal.jpg",
 "width": 800,
 "height": 600
 }
 },
 {
 "thumbnail": {
 "url": "https://uploads.justpark.com/cdn/media/uploaded/listing-photos/57b1d2660e60e-thumb.jpg",
 "width": 120,
 "height": 120
 },
 "normal": {
 "url": "https://uploads.justpark.com/cdn/media/uploaded/listing-photos/57b1d2660e60e-normal.jpg",
 "width": 800,
 "height": 600
 }
 },
 {
 "thumbnail": {
 "url": "https://uploads.justpark.com/cdn/media/uploaded/listing-photos/57b1d27d227a8-thumb.jpg",
 "width": 120,
 "height": 120
 },
 "normal": {
 "url": "https://uploads.justpark.com/cdn/media/uploaded/listing-photos/57b1d27d227a8-normal.jpg",
 "width": 800,
 "height": 600
 }
 },
 {
 "thumbnail": {
 "url": "https://uploads.justpark.com/cdn/media/uploaded/listing-photos/57b1d29938ce5-thumb.jpg",
 "width": 120,
 "height": 120
 },
 "normal": {
 "url": "https://uploads.justpark.com/cdn/media/uploaded/listing-photos/57b1d29938ce5-normal.jpg",
 "width": 800,
 "height": 600
 }
 },
 {
 "thumbnail": {
 "url": "https://uploads.justpark.com/cdn/media/uploaded/listing-photos/57b1d2b7059a0-thumb.jpg",
 "width": 120,
 "height": 120
 },
 "normal": {
 "url": "https://uploads.justpark.com/cdn/media/uploaded/listing-photos/57b1d2b7059a0-normal.jpg",
 "width": 800,
 "height": 600
 }
 },
 {
 "thumbnail": {
 "url": "https://uploads.justpark.com/cdn/media/uploaded/listing-photos/57b1d2cd84765-thumb.jpg",
 "width": 120,
 "height": 120
 },
 "normal": {
 "url": "https://uploads.justpark.com/cdn/media/uploaded/listing-photos/57b1d2cd84765-normal.jpg",
 "width": 800,
 "height": 600
 }
 }
 ],
 "description": "24 hour car park in Wembley, North West London minutes walk from Wembley Stadium\n\n- 10 minutes walk from Wembley Park Station\n- 5 minutes walk from Wembley Stadium Station\n- 2 minutes walk from London Designer Outlet (directly opposite)\n\nDetails: \n\n- Attendant on site part time\n- No height or width restrictions\n- Outside of congestion charge zone\n\nEvent days:\n\n**PLEASE READ**\n\nOn a stadium event day, the charge is £25 flat rate for the day.\nOn an arena event day, the charge is £10 flat rate for the day.\n\nSuitable for both cars and vans/minibuses (vans/minibuses will be liable for an additional £15 charge paid through JustPark - a total for the day of £40. If you are interested in parking a van or minibus, please message the JustPark team through the messaging function or make your booking and give us a call to pay the additional £15. If you do not do this, you will be liable to receive a PCN for paying the incorrect fee, or to pay the additional £15 in cash when you arrive at the car park). \n\nYou can find details of the event days on the Wembley Stadium and SSE Arena websites.",
 "streetview_data": {
 "lat": 51.557113230315,
 "lng": -0.2866062324623,
 "heading": 0,
 "pitch": 0,
 "zoom": 0
 },
 "has_anpr": true
 
*/
