//
//  HomeViewModel.swift
//  JustParkTest
//
//  Created by Carla on 2017/08/18.
//  Copyright © 2017 Carla. All rights reserved.
//

import Foundation
import CoreLocation
import GoogleMaps

protocol HomeViewModelProtocol {
    var totalNumberOfParkingSpaces: Int {get}
    func positionForParkingSpace(atIndex index: Int) -> CLLocationCoordinate2D?
    func titleForParkingSpace(atIndex index: Int) -> String
    func descriptionForParkingSpace(atIndex index: Int) -> String
    
}

class HomeViewModel : HomeViewModelProtocol {
    var parkingSpaceService : ParkingSpaceService?
    var mapService : MapService?
    var mapConfig : MapConfig { get {return (self.mapService?.config)! }}
    
    var parkingSpaces : [ParkingSpace]?
    var reload : (()->())?
    
    
    init(reloadCallback : @escaping (()->())) {
        self.mapService = MapService()
        self.parkingSpaceService = ParkingSpaceService()
        self.reload = reloadCallback
        self.retrieveData()
        
    }
    
    func descriptionForParkingSpace(atIndex index: Int) -> String {
        guard index < self.totalNumberOfParkingSpaces else { return "" }
        
        return self.parkingSpaces![index].description
    }
    
    func titleForParkingSpace(atIndex index: Int) -> String {
        guard index < self.totalNumberOfParkingSpaces else { return "" }
        
        return self.parkingSpaces![index].title
    }
    
    func positionForParkingSpace(atIndex index: Int) -> CLLocationCoordinate2D? {
        guard index < self.totalNumberOfParkingSpaces else { return nil }
        
        return self.parkingSpaces![index].location
    }
    
    var totalNumberOfParkingSpaces: Int {
        get {
            return self.parkingSpaces != nil ? self.parkingSpaces!.count : 0
        }
    }
    
    var mapConfigurationLatitude: Double {
        get { return mapConfig.latitude
        }}
    
    var mapConfigurationLongitude: Double {
        get { return mapConfig.longitude
        }}
    
}

//MARK: - Private Methods
extension HomeViewModel {
    
    
    fileprivate func retrieveData() {
        
        let parameters = ["near": ["lat": mapConfig.latitude,
                                   "lng": mapConfig.longitude,
                                   "distance": mapConfig.distance]]
        self.parkingSpaceService?.getParkingSpaces(parameters: parameters, completion: { (spaces) in
            self.parkingSpaces = spaces
            self.reload!()
        })
    }
    
    func getPathBetween(origin: CLLocationCoordinate2D, destination: CLLocationCoordinate2D, completion: @escaping (GMSPolyline) -> Void){
        self.mapService?.drawPath(origin: origin, destination: destination) { polyline in
            completion(polyline)
        }
    }

}
