//
//  HomeViewModel.swift
//  JustParkTest
//
//  Created by Carla on 2017/08/18.
//  Copyright © 2017 Carla. All rights reserved.
//

import Foundation
import CoreLocation
import GoogleMaps

class HomeViewModel {
    //MARK: - Properties
    private var parkingSpaceService : ParkingSpaceService?
    private var mapService : MapService?
    private var mapConfig : MapConfig { get {return (self.mapService?.config)! }}
    private var parkingSpaces : [ParkingSpace]?
    private var reload : (()->())?
    
    
    //MARK: - Initialize
    init(reloadCallback : @escaping (()->())) {
        self.mapService = MapService()
        self.parkingSpaceService = ParkingSpaceService()
        self.reload = reloadCallback
        self.retrieveData()
        
    }
    
    //MARK: - Public API
    func nextViewModel(forMarkerAtPosition position:CLLocationCoordinate2D) -> MoreInfoViewModel {
        let parkingSpaces = self.parkingSpaces?.filter() {
            return $0.location.latitude == position.latitude && $0.location.longitude == position.longitude
        }
        
        return MoreInfoViewModel(parkingSpace : parkingSpaces![0])
    }
    
    var totalNumberOfParkingSpaces: Int {
        get {
            return self.parkingSpaces != nil ? self.parkingSpaces!.count : 0
        }
    }
    
    func getPathBetween(origin: CLLocationCoordinate2D, destination: CLLocationCoordinate2D, completion: @escaping (GMSPolyline) -> Void){
        self.mapService?.drawPath(origin: origin, destination: destination) { polyline in
            completion(polyline)
        }
    }
    
    //MARK: Properties for ViewController
    var mapConfigurationLatitude: Double { get { return mapConfig.latitude } }
    
    var mapConfigurationLongitude: Double { get { return mapConfig.longitude } }
    
    func descriptionForParkingSpace(atIndex index: Int) -> String {
        guard index < self.totalNumberOfParkingSpaces else { return "" }
        
        return self.parkingSpaces![index].description
    }
    
    func titleForParkingSpace(atIndex index: Int) -> String {
        guard index < self.totalNumberOfParkingSpaces else { return "" }
        
        return self.parkingSpaces![index].title
    }
    
    func positionForParkingSpace(atIndex index: Int) -> CLLocationCoordinate2D? {
        guard index < self.totalNumberOfParkingSpaces else { return nil }
        
        return self.parkingSpaces![index].location
    }
    
    func priceForParkingSpace(atIndex index: Int) -> String? {
        guard index < self.totalNumberOfParkingSpaces else { return nil }
        
        return String(self.parkingSpaces![index].price)
    }
    
    
    //MARK: - Private Methods
    private func retrieveData() {
        
        let parameters = ["near": ["lat": mapConfig.latitude,
                                   "lng": mapConfig.longitude,
                                   "distance": mapConfig.distance]]
        self.parkingSpaceService?.getParkingSpaces(parameters: parameters, completion: { (spaces) in
            self.parkingSpaces = spaces
            self.reload!()
        })
    }
    
}
