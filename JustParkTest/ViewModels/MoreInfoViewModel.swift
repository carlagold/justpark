//
//  MoreInfoViewModel.swift
//  JustParkTest
//
//  Created by Carla on 2017/08/20.
//  Copyright © 2017 Carla. All rights reserved.
//

import Foundation


struct MoreInfoViewModel {
    
    private var parkingSpace : ParkingSpace?
    
    var title : String? {get { return parkingSpace?.title }}
    var review : Double? {get { return parkingSpace?.review }}
    var reviewCount : Int? {get { return parkingSpace?.reviewCount }}
    var distance : Double? {get { return parkingSpace?.distance }}
    var description : String? {get { return parkingSpace?.description }}
    
    init(parkingSpace : ParkingSpace) {
        self.parkingSpace = parkingSpace
    }
    
    
    
}
